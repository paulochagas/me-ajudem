import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Switch } from 'react-router-dom';
import { Route } from 'react-router-dom';
import './style.css';
import Register from './pages/register/index';

export default class App extends Component {

  render() {
    
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route patch = "/register" component= {Register}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}


