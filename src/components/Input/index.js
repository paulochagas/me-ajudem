import './style.css';
import React from 'react';
export default function Input(props) {
    return (
        <input 
            type={props.type}
            className={props.className}
            placeholder={props.placeHolder}
            value={props.value} />
    )
}