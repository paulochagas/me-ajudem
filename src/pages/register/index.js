import React, { Component } from 'react';
import Input from '../../components/Input/index';
import  './style.css';

export default class Register extends Component {

    render() {
        const inputs = [
            {
              placeHolder: 'Nome',
              type: 'text'
            },
            {
              placeHolder: 'Telephone',
              type: 'text'
            },
            {
              placeHolder: 'Senha',
              type: 'text'
            },
            {
              placeHolder: 'Email',
              type: 'text'
            },
            {
              placeHolder: 'Estado',
              type: 'text'
            },
            {
                placeHolder: 'Cidade',
                type: 'text'
            },
            {
              placeHolder: 'Rua',
              type: 'text'
            },
          ];

        var inputsList = inputs.map((input) => {
            return <Input type={input.type} className="input-1" placeHolder={input.placeHolder} value={input.value}/>
        })
        return(
            <div className="container">
                <h1>Crie<br/>Sua<br/>Conta</h1>
                <div className="inputs">
                    {inputsList}
                </div>
                <button>Criar conta</button>
            </div>

        )
    }

}